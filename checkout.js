var app;

app = angular.module("checkout", ["ngRoute", "ngResource", "ngAnimate", "ui.bootstrap"]);

app.config(function($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "/app/templates/checkout/index.html",
        controller: "indexCheckoutController"
    });
});