app.controller('indexCheckoutController', function($scope, CheckoutFactory) {
    $scope.showSuccessAlert = false;
    $scope.switchBool = function(value){
        $scope[value] = !$scope[value];
    };

    $scope.removeFromCheckout = function(index) {
        console.log($scope.items);
        $scope.items.splice(index, 1);
        $scope.items.splice(index - 1, 1);
    };
    $scope.sendOrder = function() {
        return CheckoutFactory.sendOrder($scope.items, $scope.comments).success(function(data) {
            console.log(data);
            $scope.successText = "Η Παραγγελία σας στάλθηκε!";
            $scope.showSuccessAlert = true;
        });
    };
    CheckoutFactory.getCheckout().success(function(data) {
        $scope.items = data;
    });
});