// Category View Controller
app.controller('indexCategoriesController', function($scope,ProductFactory){
    ProductFactory.getCategories().success(function(data){
        $scope.categories = data;
    });
});

//Product List Controller (Pagination)
app.controller('showCategoriesController', function($scope,$routeParams,ProductFactory,FilterFactory){

    //Init Ajax request and pagination for product list (Depends on url /category/:categoryid/:manufacturerid)
    FilterFactory.getProducts($routeParams.categoryid,$routeParams.groupid,$routeParams.subgroupid,$routeParams.seasonid,$routeParams.denim).success(function(response){

        // Product data after ajax success event
        $scope.products = response.data,
        // Set the current page of product list
        $scope.currentPage = response.current_page,
        // Set the products per page
        $scope.numPerPage =  response.per_page,
        // Set the max visible pagination buttons
        $scope.maxSize = 5,
        // Set the total number of products
        $scope.totalItems = response.total;
        // Set the total number of pages
        $scope.numPages =  response.last_page;

        // Watch for page change event
        $scope.$watch('currentPage + numPerPage', function() {

            var idone = $scope.selectedProductId;
            var title = $scope.selectedProductTitle;


            // Call the ajax to laravel with new page
            FilterFactory.getProducts($routeParams.categoryid,$routeParams.groupid,$routeParams.subgroupid,$routeParams.seasonid,$routeParams.denim,null,$scope.currentPage).success(function(response){
                // Return the data to ng-repeat
                $scope.products = response.data;
            });
        });

        // Alias to Factory for product add to cart action
        $scope.addToCart = function(id, selectedSize, selectedColor, cart_add){
           ProductFactory.addToCart(id, selectedSize, selectedColor, cart_add);
        };
        // Alias to Factory for add to favorites action
        $scope.addToFavorites = function(id, selectedSize, selectedColor, user_id, favorite_add){ ProductFactory.addToFavorites(id, selectedSize, selectedColor, user_id, favorite_add); };

    });
});

// Filters Controller
app.controller('filterController',function($scope,$routeParams,ProductFactory,FilterFactory,$location){
    // Ajax init for categories list
    ProductFactory.getCategoriesl().success(function(data) { $scope.categoriesl = data; });
    // Ajax init for manufacturers list
    ProductFactory.getGroups().success(function(data) { $scope.groups = data; });

    ProductFactory.getSubGroups().success(function(data) { $scope.subgroups = data; });

    ProductFactory.getSeasons().success(function(data) { $scope.seasons = data; });

    $scope.denim = [ {id: 0, title: 'Επιλέξτε Κατάστημα'},
                     {id: 1, title: 'EDWARD'},
                     {id: 2, title: 'DENIMUNITED'}
                   ];

    // Change the selected category option if categoryid exists on the path
    $scope.selectedCategory = typeof($routeParams.categoryid) == 'undefined' ? 0 : $routeParams.categoryid;
    // Change the selected category option if groupid exists on the path
    $scope.selectedGroup = typeof($routeParams.groupid) == 'undefined' ? 0 :  $routeParams.productid;
    // Change the selected category option if subgroupid exists on the path
    $scope.selectedSubGroup = typeof($routeParams.subgroupid) == 'undefined' ? 0 :  $routeParams.subgroupid;
    // Change the selected category option if seasonid exists on the path
    $scope.selectedSeason = typeof($routeParams.seasonid) == 'undefined' ? 0 :  $routeParams.seasonid;
    // Change the selected category option if denim exists on the path
    $scope.selectedDenim = typeof($routeParams.denim) == 'undefined' ? 0 :  $routeParams.denim;


    $scope.filterChanged = function(selectedCategory,selectedGroup,selectedSubGroup,selectedSeason,selectedDenim){

          $location.path('/category/' + selectedCategory + '/' + selectedGroup + '/' + selectedSubGroup + '/' + selectedSeason + '/' + selectedDenim);

    };

    $scope.productFilterChanged = function(productId){
        FilterFactory.getProducts($routeParams.categoryid, $routeParams.groupid,$routeParams.subgroupid,$routeParams.seasonid,$routeParams.denim,productId).success(function(response){
                // Product data after ajax success event
                $scope.products = response.data;
                if(response.total > 1){
                    $scope.hasResults = true;
                }else{
                    $scope.hasResults = false;
                }
        });
    };
});

app.controller('showProductController',function($scope,$routeParams,ProductFactory,FilterFactory){
    FilterFactory.getProduct($routeParams.productid).success(function(response){
       $scope.product = response;
    });
});
