app.factory("CheckoutFactory", function($http) {
    var factory;
    factory = {};
    factory.getCheckout = function() {
        return $http.get('/ajax/checkout');
    };
    factory.sendOrder = function(items, comments) {
        return $http.post('/ajax/order', {
            products: items,
            comments: comments
        });
    };
    return factory;
});