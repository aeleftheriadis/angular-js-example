//Register the Product Factory aka Model
app.factory('ProductFactory', function($http, $route){

    // Init the factory object
    var factory = {};

    //Get groups list for filters dropdown
    factory.getGroups = function(){
        return $http.get('/ajax/groups');
    };
    //Get subgroups list for filters dropdown
    factory.getSubGroups = function(){
        return $http.get('/ajax/subgroups');
    };
    //Get seasons list for filters dropdown
    factory.getSeasons = function(){
        return $http.get('/ajax/seasons');
    };

    //Get categories list for category list page
    factory.getCategories = function () {
        return $http.get('/ajax/categories/');
    };

    //Get categories list for filters dropdown
    factory.getCategoriesl = function () {
        return $http.get('/ajax/categoriesl/');
    };

    //Add a product to cart and return a success message
    factory.addToCart = function(id, selectedSize, selectedColor, cart_add) {
        url = '/cart/create?id='+id+'&size='+selectedSize+'&color='+selectedColor;

        $http.get(url).success(function(data) {
            if (data.success==false) {
                // if not successful, bind errors to error variables
                $( "#reload" ).html();
                $(".alert-error").html(data.errors).stop().fadeIn(400).delay(2000).fadeOut(400);
            }
            else{
                $( "#reload" ).html('<i class="icon-shopping-cart"></i> ' + data.cart_text);
                $(".alert-success").html(cart_add).stop().fadeIn(400).delay(2000).fadeOut(400);
            }
        });
    };

    //Add a product to favorites and return a success message (user must be logged in)
    factory.addToFavorites = function(id, selectedSize, selectedColor, user_id, favorite_add) {
        url = '/favorite/create?id='+id+'&size='+selectedSize+'&color='+selectedColor+'&user_id='+user_id;
        $http.get(url).success(function(data){

            if (data.success==false) {
                // if not successful, bind errors to error variables
                $( "#reload_fav" ).html();
                $(".alert-error").html(data.errors).stop().fadeIn(400).delay(2000).fadeOut(400);
            }
            else{
                $( "#reload_fav" ).html('<i class="icon-heart"></i> ' + data.favorite_text);
                $(".alert-success").html(favorite_add).stop().fadeIn(400).delay(2000).fadeOut(400);
            }

        });
    };
    //Return the main object to app
    return factory;
});

//Register the Filter Factory aka Model
app.factory('FilterFactory', function($http){

    //Init the factory object
    var factory = {};

    //Returns the product list after filters and pagination
    factory.getProducts = function (category,group,subgroup,season,denim,id,page) {
        var url = '/ajax/products';
//        var or = null;
//        var iid = null;
//        var oprator = null;
        var fdenim = null;
        var doprator = null;

        if(category <= 0){ fcategory = null; }else{ fcategory = category; }

        if(group <= 0){ fgroup = null; }else{ fgroup = group; }

        if(subgroup <= 0){ fsubgroup = null; }else{ fsubgroup = subgroup; }

        if(season <= 0){ fseason = null; }else{ fseason = season; }

        if (denim == 1) {
            fdenim = 'DU.%'; doprator = "not like";
        } else if (denim == 2) {
            fdenim = 'DU.%'; doprator = "like";
        }

//        if(id) {
//            or = "or";
//            if(id.length > 1 ){
//                iid = id.replace("*","%");
//                iid = iid.replace("*","%");
//            }
//        }
//        if(id){ oprator = "like"; }

        return $http({
                url: url,
                method: "GET",
                params: {cat_id: fcategory, group_id: fgroup,subgroup_id: fsubgroup,season_id:fseason,title:fdenim,"operator[title]":doprator,page: page},
                cache: false
            });
    };

    factory.getProduct = function(id){
       return $http({
           url: '/ajax/product',
           metod: "GET",
           params: {id: id},
           cache: false
       });
    };

    //Return the main object to app
    return factory;
});