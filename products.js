var app = angular.module('product',['angucomplete','ngRoute','ngResource','ngAnimate','ui.bootstrap']);

app.config(function($routeProvider) {

    $routeProvider
        .when('/',{
            templateUrl: '/app/templates/product/categories.html',
            controller: 'indexCategoriesController'
        })
        .when('/category/:categoryid/:groupid/:subgroupid/:seasonid/:denim', {
            templateUrl: '/app/templates/product/products.html',
            controller: 'showCategoriesController'
        })
        .when('/product/:productid',{
            templateUrl: '/app/templates/product/product.html',
            controller: 'showProductController'
        })

});