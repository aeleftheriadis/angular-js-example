var app = angular.module('profile',['ngRoute','ngResource','ngAnimate','ngSanitize','ui.bootstrap']);

app.config(function($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: "/app/templates/profile/index.html",
            controller: "PurchasesController"
        }).when("/edit/profile", {
            templateUrl: "/app/templates/profile/edit.html",
            controller: "ProfileController"
        }).when("/favorites/profile", {
            templateUrl: "/app/templates/profile/favorites.html",
            controller: "ProfileController"
        }).when("/orders/completed", {
            templateUrl: "/app/templates/profile/index.html",
            controller: "PurchasesController"
        }).when("/orders/completed/:page", {
            templateUrl: "/app/templates/profile/index.html",
            controller: "PurchasesController"
        }).otherwise("/orders/completed", {
            templateUrl: "/app/templates/profile/index.html",
            controller: "PurchasesController"
    });

});